
module.exports = {
    connections: [
        {
            name: "store",
            config: {
                connectionLimit: 1100,
                host: "localhost",
                port: 3306,
                user: "store_manager_user",
                password: "+e6ed!x?6&95YH_",
                database: "store_manager",
                dateStrings: "date",
            },
        },
    ],
    sessionConfig: {
        connectionLimit: 2,
        host: "localhost",
        port: 3306,
        user: "store_sessions_user",
        password: "$+L3ZsZY@m@vM9gYNu",
        database: "express_session",
        // The maximum age of a valid session; milliseconds:
        maxAgeMilliseconds: 86400000,
        tableName: "store_sessions",
    },
};
