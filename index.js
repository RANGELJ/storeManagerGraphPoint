const express = require("express");
const expressGraphQL = require("express-graphql");
const http = require("http");

const app = express();
require("./local_modules/session_builder")(app);
const graphErrorLoger
    = require("./local_modules/error_loger")("graph_resolve_errors.log");

app.use("/", expressGraphQL(req => ({
    schema: require("./squema/zroot"),
    graphiql: true,
    context: {
        db: require("./local_modules/mysql/db"),
        logError: graphErrorLoger,
        session: req.session,
        throwError: (message) => {
            throw {noLog: true, message};
        },
        mail: require("./local_modules/mailer"),
        knex: require("knex")({
            client: "mysql",
            connection: {
                host: "localhost",
                port: 3306,
                user: "store_manager_user",
                password: "+e6ed!x?6&95YH_",
                database: "store_manager",
                dateStrings: "date",
            },
            pool: {min: 0, max: 7},
        }),
    },
})));

http.createServer(app).listen(8181, () => {
    console.log("Server runing on port 8181... "); // eslint-disable-line
});
