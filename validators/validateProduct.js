module.exports = async (args, context, isNew) => {
    const precioReguex = /^[0-9]{1,10}(\.[0-9]{1,2})?$/;

    if (args.descripcion.length < 1) {
        context.throwError("Descripcion de producto invalida");
    } else if (args.descripcion.length > 100) {
        context.throwError("Descripcion de producto muy largo");
    } else if (args.costoCompra < 0) {
        context.throwError("Costo de compra no puede ser negativo");
    } else if (args.costoVenta < 0) {
        context.throwError("Costo de venta no puede ser negativo");
    } else if (! precioReguex.test(args.costoCompra + "") ) {
        context.throwError("Costo de compra invalido");
    } else if (! precioReguex.test(args.costoVenta + "") ) {
        context.throwError("Costo de venta invalido");
    }

    if (isNew) {
        const productoConDescripcion = await context.knex
            .select("id")
            .from("productos")
            .where({
                descripcion: args.descripcion,
                idOwner: context.session.idOwner,
            }).first();

        if (productoConDescripcion !== undefined) {
            context.throwError("Un producto con esta descripcion ya existe");
        }
    }

    return true;
};
