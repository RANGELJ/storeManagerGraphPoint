
const nombreReguex = /^[A-Za-z][A-Za-z0-9]{1,25}$/;

module.exports = function(args, context) {
    return {
        async nombre() {
            if (! nombreReguex.test(args.nombre)) {
                context.throwError("Nombre de almacen invalido");
                return true;
            }
        },
        async idSucursal() {
            const {idSucursal} = await context.knex
                .select("id AS idSucursal")
                .from("sucursales")
                .where({
                    id: args.idSucursal,
                    idOwner: context.session.idOwner,
                    activo: 1,
                })
                .first();

            if (! idSucursal) {
                context.throwError("Sucursal invalida");
            }
            console.log(idSucursal);//eslint-disable-line
            return true;
        },
    };
};
