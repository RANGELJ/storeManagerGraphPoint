
const regexSucursalNombre = /^[A-Za-z][A-Za-z0-9]{2,20}$/;

module.exports = function(root, args, context) {
    return {
        async nombre() {
            if ( ! regexSucursalNombre.test(args.nombre) ) {
                context.throwError("Nombre de sucursal invalido");
            }
        },
    };
};
