
-- @byEmail
SELECT
    id
FROM
    usuarios
WHERE
    email = ?
    AND activo = 1
-- @

-- @byUserNameLike
SELECT
    id
FROM
    usuarios
WHERE
    `key` LIKE ?
    AND activo = 1
-- @

-- @createUser
INSERT INTO usuarios
(`key`, hash, salt, nombre, apellidoPaterno, apellidoMaterno,
    email, correoVerificado, activo, tipoUsuario)
VALUES
(?, ?, ?, ?, ?, ?, ?, 0, 1, 1)
-- @

-- @registerEmailVerLink
INSERT INTO email_verification_links
(idUser, hash, salt, activo)
VALUES
(?, ?, ?, 1)
-- @

-- @consultarEmailVerLink
SELECT
    verifier.hash,
    user.email
FROM
    email_verification_links verifier
JOIN
    usuarios user ON (
        user.id = verifier.idUser
    )
WHERE
    verifier.activo = 1
    AND user.activo = 1
    AND user.correoVerificado = 0
    AND verifier.idUser = ?
-- @

-- @markEmailVerified
UPDATE usuarios
    SET correoVerificado = 1
WHERE
    id = ?
-- @

-- @getUserById
SELECT
    id,
    `key` AS username
FROM
    usuarios
WHERE
    id = ?
    AND activo = 1
-- @
