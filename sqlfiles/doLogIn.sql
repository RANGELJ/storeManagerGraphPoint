-- @getUserByUser
SELECT
    id,
    hash,
    salt
FROM
    usuarios
WHERE
    `key` = ?
    AND activo = 1
-- @