
const mysql = require("mysql2/promise");
const config = require("../../mysql.conf");

pools = {};

config.connections.forEach(connection => {
    pools[connection.name] = mysql.createPool(connection.config);
});

module.exports = async function(connectionName) {
    /** Extrae una coneccion del pool de conexxiones */
    const connection = await pools[connectionName].getConnection();

    /** Agrega el metodo de execute con parametros opcionales */
    connection.executeWithOptionalParams
        = executeWithOptionalParamsFactory(connection);

    return connection;
};

const executeWithOptionalParamsFactory = function(connection) {
    return async function(sql, {optionalParams, params, endSQL}) {
        params = params ? params : [];
        optionalParams = optionalParams ? optionalParams : [];
        endSQL = endSQL ? endSQL : "";
        let where = "";
        optionalParams.forEach(optionalParam => {
            const optionalValue = optionalParam[0];
            const optionalSQL = optionalParam[1];
            if (optionalValue) {
                where += optionalSQL;
                params.push(optionalValue);
            }
        });
        return await connection.execute(sql + where + endSQL, params);
    };
};
