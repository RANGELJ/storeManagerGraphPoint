
const regexValidatorReference = /validate\["[^]*?"\]\([^]*?\)\s*;/g;
const regexValidatorStart = /validate\["[^]*?"\]/g;
const regexValidatorParameters = /\([^]*?\)\s*;/g;

module.exports = function(source, map, meta) {
    const callback = this.async();
    const validatorMatches = source.match(regexValidatorReference);

    if (validatorMatches !== null) {
        validatorMatches.forEach(validatorString => {
            let [startString] = validatorString.match(regexValidatorStart);
            startString = startString.replace(/^validate\["/g, "")
                .replace(/"]$/, "");
            let [parameters]
                = validatorString.match(regexValidatorParameters);
            parameters = parameters.replace(/\s*;$/g, "");

            const requireString = `await `
                + `require("../validators/${startString}")`
                + `${parameters};//eslint-disable-line`;

            source = source.replace(validatorString, requireString);
        });
    }

    callback(null, source, map, meta);
};
