
module.exports = function(source, map, meta) {
    const callback = this.async();

    const reguexMiddleware = /middleware\s*:\s*\[[^\]]+\]\s*,/g;

    const matches = source.match(reguexMiddleware);

    if (matches === null) {
        callback(null, source, map, meta);
        return;
    } else if (matches.length > 1) {
        callback({message: "No debe haber mas de un array middleware"}
            , source, map, meta);
    }

    callback(null, source, map, meta);
};
