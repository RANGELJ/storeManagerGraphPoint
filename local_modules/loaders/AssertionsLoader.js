
module.exports = function(source, map, meta) {
    const callback = this.async();

    const assertionReguex = /assert\s*\[[^\]]+\]\s*;/g;

    const matches = source.match(assertionReguex);

    if (matches === null || matches.length === 0) {
        callback(null, source, map, meta);
        return;
    } else if (matches.length > 1) {
        callback({
            message: "No debe haber mas de una lista assert",
        }, source, map, meta);
        return;
    }

    const listaAsserts = matches[0]
        .replace(/assert\s*\[/g, "")
        .replace(/\]\s*;/g, "")
        .match(/"[^"]+"/g)
        .map(assert => assert.replace(/^"/g, "").replace(/"$/g, "") );

    let assertionsRequire = "";
    listaAsserts.forEach(assert => {
        assertionsRequire += `await require("../assertions/${assert}")`
            + `(arguments[0], arguments[1], arguments[2]);`
            + `// eslint-disable-line\n`;
    });

    const newSource = source.replace(/assert\s*\[[^\]]+\]\s*;/g
        , assertionsRequire);

    callback(null, newSource, map, meta);
};
