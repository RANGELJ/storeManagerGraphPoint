const fileSystem = require("fs");

const formatToTwoDigitsAtLeast = function(value) {
    return (value < 10) ? "0" + value : value;
};

module.exports = function(logFileName) {
    return function(errorMessage) {
        let today = new Date();
        const year = today.getFullYear();
        const mont = formatToTwoDigitsAtLeast(today.getMonth() + 1);
        const day = formatToTwoDigitsAtLeast(today.getDate());
        const hours = formatToTwoDigitsAtLeast(today.getHours());
        const minutes = formatToTwoDigitsAtLeast(today.getMinutes());
        const seconds = formatToTwoDigitsAtLeast(today.getSeconds());
        today = [year, mont, day].join("-") + " " +
            [hours, minutes, seconds].join(":");
        const errorBody = `[${today}]\n${errorMessage}\n`;
        const filePath = __dirname + "/logs/" + logFileName;
        fileSystem.appendFile(filePath, errorBody, function(error) {
        });
    };
};
