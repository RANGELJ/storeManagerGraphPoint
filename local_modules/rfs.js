
const fs = require("fs");

module.exports = {
    readFile(path, encoding){
        return new Promise((resolve, reject) =>{
            fs.readFile(path, encoding, (error, fileData) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(fileData);
                }
            });
        });
    },
};
