
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
        user: "rangelstoremanagersystem@gmail.com",
        pass: "7a#$cH<..",
    },
});

module.exports = function(mailOptions) {
    mailOptions.from = "rangelstoremanagersystem@gmail.com";

    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                reject(error);
            } else {
                resolve(info.response);
            }
        });
    });
};
