
const validateProduct = require("../validators/validateProduct");

module.exports = {
    type: "@BooleanFlag",
    args: {
        id: {type: "@Int"},
        descripcion: {type: "@String"},
        costoCompra: {type: "@Float"},
        costoVenta: {type: "@Float"},
    },
    async resolve(root, args, context) {
        // @Error al actualizar el producto
        await validateProduct(args, context);

        await context.knex("productos")
            .update({
                descripcion: args.descripcion,
                costoCompra: args.costoCompra,
                costoVenta: args.costoVenta,
            })
            .where({
                id: args.id,
            });

        return {flag: true};
    },
};
