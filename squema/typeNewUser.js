module.exports = new GraphQLObjectType({
    name: "typeNewUser",
    description: "The result of a sign in action",
    fields: () => ({
        username: {type: "@String"},
    }),
});
