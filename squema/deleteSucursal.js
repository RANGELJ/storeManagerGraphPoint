module.exports = {
    type: "@BooleanFlag",
    args: {
        id: {type: "@Int"},
    },
    async resolve(root, args, context) {
        // @Error al borrar la sucursal
        assert["isLogedIn"];

        await context.knex("sucursales")
            .where({
                id: args.id,
                idUsuario: context.session.idUser,
            })
            .update({
                activo: 0,
            });

        return {flag: true};
    },
};
