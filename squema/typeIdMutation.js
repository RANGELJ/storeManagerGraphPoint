module.exports = new GraphQLObjectType({
    name: "typeIdMutation",
    fields: () => ({
        id: {type: "@Int"},
    }),
});
