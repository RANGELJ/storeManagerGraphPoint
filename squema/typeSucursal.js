module.exports = new GraphQLObjectType({
    name: "typeSucursal",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
        almacenes: {
            type: "@List(Almacen)",
            async resolve(sucursal, args, context) {
                // @Error al consultar los almacenes de esta sucursal
                const almacenes = await context.knex
                    .select("id", "nombre")
                    .from("almacenes")
                    .where({
                        idSucursal: sucursal.id,
                    });
                return almacenes;
            },
        },
    }),
});
