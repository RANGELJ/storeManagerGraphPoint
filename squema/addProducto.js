
const validateProduct = require("../validators/validateProduct");

module.exports = {
    type: "@IdMutation",
    args: {
        descripcion: {type: "@String"},
        costoCompra: {type: "@Float"},
        costoVenta: {type: "@Float"},
    },
    async resolve(root, args, context) {
        // @Error al dar de alta el producto
        assert["isLogedIn"];

        await validateProduct(args, context);

        const idProducto = await context.knex("productos")
            .insert({
                descripcion: args.descripcion,
                costoCompra: args.costoCompra,
                costoVenta: args.costoVenta,
                activo: 1,
                momentoAlta: context.knex.fn.now(),
                idUsuarioAlta: context.session.idUser,
                idOwner: context.session.idOwner,
            });

        return {id: idProducto};
    },
};
