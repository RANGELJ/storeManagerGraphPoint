module.exports = {
    type: "@BooleanFlag",
    args: {
        id: {type: "@Int"},
    },
    async resolve(root, args, context) {
        // @Error al eliminar el producto
        assert["isLogedIn"];
        await context.knex("productos")
            .update({
                activo: 0,
            })
            .where({
                id: args.id,
                idOwner: context.session.idOwner,
            });
        return {flag: true};
    },
};
