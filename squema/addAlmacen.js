module.exports = {
    type: "@IdMutation",
    args: {
        idSucursal: {type: "@Int"},
        nombre: {type: "@String"},
    },
    async resolve(root, args, context) {
        // @Error al registrar el almacen
        assert["isLogedIn"];

        const validator
            = require("../validators/validatorAlmacenes")(args, context);
        await validator.nombre();
        await validator.idSucursal();

        return {id: 1515};
    },
};
