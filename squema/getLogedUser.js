module.exports = {
    type: "@User",
    sqlfiles: ["users"],
    async resolve(root, args, context) {
        // @Error al consultar tus datos de usuario
        assert["isLogedIn"];

        const conn = await context.db("store");
        const [users] = await conn.execute("sql@getUserById", [
            context.session.idUser,
        ]);

        if (users.length !== 1) {
            context.throwError("Usuario no encontrado");
        }

        return users[0];
    },
};
