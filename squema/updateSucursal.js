module.exports = {
    type: "@BooleanFlag",
    args: {
        id: {type: "@Int"},
        nombre: {type: "@String"},
    },
    async resolve(root, args, context) {
        // @Error al actualizar la sucursal
        assert["isLogedIn"];
        const validator
            = require("../validators/validatorSucursales")(root, args, context);
        validator.nombre();

        await context.knex("sucursales")
            .where({
                id: args.id,
                idUsuario: context.session.idUsuario,
            })
            .update({
                nombre: args.nombre,
            });
    },
};
