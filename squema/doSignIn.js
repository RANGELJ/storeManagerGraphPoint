module.exports = {
    type: "@NewUser",
    args: {
        nombre: {type: "@NonNull(String)"},
        apellidoPaterno: {type: "@NonNull(String)"},
        apellidoMaterno: {type: "@NonNull(String)"},
        email: {type: "@NonNull(String)"},
        password: {type: "@NonNull(String)"},
        confirmPassword: {type: "@NonNull(String)"},
    },
    sqlfiles: ["users"],
    async resolve(root, args, context) {
        // @Error al registrar el usuario
        assert["notLogedIn", "validUserInfo"];

        let newUsername = args.nombre.charAt(0)
            + args.apellidoPaterno.split(/\s+/g)[0].toUpperCase()
            + args.apellidoMaterno.charAt(0);

        const conn = await context.db("store");
        const [usersByName] = await conn.execute("sql@byUserNameLike", [
            "%" + newUsername + "%",
        ]);

        if (usersByName.length > 0) {
            newUsername += usersByName.length;
        }

        await context.knex.transaction(async (transaction) => {
            const hasher = require("../local_modules/hasher");
            let hasherRes = await hasher.hash(args.password);

            const idNewUser = await context.knex("usuarios")
                .transacting(transaction)
                .insert({
                    key: newUsername,
                    hash: hasherRes.hash,
                    salt: hasherRes.salt,
                    nombre: args.nombre,
                    apellidoPaterno: args.apellidoPaterno,
                    apellidoMaterno: args.apellidoMaterno,
                    email: args.email,
                    correoVerificado: 0,
                    tipoUsuario: 1,
                    activo: 1,
                });

            hasherRes = await hasher.hash(args.email);
            let linkSalt = hasherRes.salt;
            await context.knex("email_verification_links")
                .transacting(transaction)
                .insert({
                    idUser: idNewUser,
                    hash: hasherRes.hash,
                    salt: linkSalt,
                    activo: 1,
                });

            await context.knex("usuarios")
                .transacting(transaction)
                .update({
                    idUsuarioOwner: idNewUser,
                })
                .where({
                    id: idNewUser,
                });

            await context.mail({
                to: args.email,
                subject: "Verificacion de cuenta de correo",
                html: `
                    <h1>Por favor ingresa a este link para verificar `
                    + `tu cuenta:</h1>
                    <a href="url?ur=${idNewUser}&hs=${linkSalt}"
                        >verificar cuenta..</a>
                `,
            });
        });

        return {username: newUsername};
    },
};
