module.exports = {
    type: "@BooleanFlag",
    args: {
        idUser: {type: "@Int"},
        token: {type: "@String"},
    },
    sqlfiles: ["users"],
    async resolve(root, args, context) {
        // @Error al verificar el correo electronico
        const conn = await context.db("store");
        const [users] = await conn.execute("sql@consultarEmailVerLink", [
            args.idUser,
        ]);
        if (users.length !== 1) {
            context.throwError("Error, credenciales invalidas");
        }

        const user = users[0];

        const hasher = require("../local_modules/hasher");
        const validHash = hasher.verify(user.email, args.token, user.hash);

        if (validHash) {
            await conn.execute("sql@markEmailVerified", [args.idUser]);
        }

        return {flag: validHash};
    },
};
