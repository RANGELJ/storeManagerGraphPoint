module.exports = new GraphQLObjectType({
    name: "typeProducto",
    fields: () => ({
        id: {type: "@Int"},
        descripcion: {type: "@String"},
        costoCompra: {type: "@Float"},
        costoVenta: {type: "@Float"},
    }),
});
