module.exports = {
    type: "@List(Sucursal)",
    async resolve(root, args, context) {
        // @Error al consultar las sucursales
        assert["isLogedIn"];

        const sucursales = await context.knex("sucursales")
            .select("id", "nombre")
            .where({
                activo: 1,
                idOwner: context.session.idOwner,
            });

        return sucursales;
    },
};
