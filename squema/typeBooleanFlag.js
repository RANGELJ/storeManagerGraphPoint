module.exports = new GraphQLObjectType({
    name: "booleanFlag",
    fields: () => ({
        flag: {type: "@Boolean"},
    }),
});
