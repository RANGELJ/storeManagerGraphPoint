module.exports = new GraphQLObjectType({
    name: "typeAlmacen",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
    }),
});
