module.exports = {
    type: "@BooleanFlag",
    args: {
        username: {type: "@NonNull(String)"},
        password: {type: "@NonNull(String)"},
    },
    sqlfiles: ["doLogIn"],
    async resolve(root, args, context) {
        // @Error al iniciar sesion
        assert["notLogedIn"];

        const user = await context.knex
            .select("id", "salt", "hash", "idUsuarioOwner",
                "tipoDeCuenta", "tipoUsuario")
            .from("usuarios")
            .where({
                key: args.username,
            })
            .first();

        if (! user) {
            context.session.idUser = undefined;
            context.throwError("Usuario o password invalido");
        }

        const hasher = require("../local_modules/hasher");
        const validUser = await hasher.verify(args.password,
            user.salt, user.hash);

        if (! validUser) {
            context.session.idUser = undefined;
            context.throwError("Usuario o password invalido");
        }

        context.session.idUser = user.id;
        context.session.idOwner = user.idUsuarioOwner;
        context.session.accountType = user.tipoDeCuenta;
        context.session.userType = user.tipoUsuario;
        return {flag: true};
    },
};
