module.exports = {
    type: "@BooleanFlag",
    async resolve(root, args, context) {
        // @Error al identificar el usuario
        console.log(context.session); //eslint-disable-line
        return {
            flag: context.session.idUser !== undefined
                && context.session.idOwner !== undefined,
        };
    },
};
