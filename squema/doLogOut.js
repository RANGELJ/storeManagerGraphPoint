module.exports = {
    type: "@BooleanFlag",
    async resolve(root, args, context) {
        // @Error al cerrar la sesion
        assert["isLogedIn"];
        context.session.idUser = undefined;
        context.session.idOwner = undefined;
        return {flag: true};
    },
};
