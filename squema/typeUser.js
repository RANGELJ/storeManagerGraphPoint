module.exports = new GraphQLObjectType({
    name: "userType",
    fields: () => ({
        id: {type: "@Int"},
        username: {type: "@String"},
    }),
});
