module.exports = {
    type: "@IdMutation",
    args: {
        nombre: {type: "@String"},
    },
    async resolve(root, args, context) {
        // @Error al registrar la sucursal
        assert["isLogedIn", "isOwner"];
        const validator
            = require("../validators/validatorSucursales")(root, args, context);
        validator.nombre();

        const {numeroSucursalesMaximas} = await context.knex
            .select("valor AS numeroSucursalesMaximas")
            .from("tipos_de_cuentas_reglas_valores")
            .where("idTipoCuenta", context.session.accountType)
            .where("idRegla", 1)
            .first();

        const {totalSucursales} = await context.knex("sucursales")
            .count({totalSucursales: "id"})
            .where({activo: 1, idOwner: context.session.idOwner})
            .first();

        if (totalSucursales >= parseInt(numeroSucursalesMaximas) ) {
            context.throwError(`No puedes dar de alta mas `
                + `de ${numeroSucursalesMaximas} sucursal(es)`);
        }

        const {sucursalesConEsteNombre} = await context.knex("sucursales")
            .count({sucursalesConEsteNombre: "id"})
            .where({activo: 1, nombre: args.nombre})
            .first();

        if (sucursalesConEsteNombre > 0) {
            context.throwError("Ya hay una sucursal con este nombre");
        }

        const idSucursal = await context.knex("sucursales")
            .insert({
                nombre: args.nombre,
                idOwner: context.session.idOwner,
                activo: 1,
            });

        return {id: idSucursal};
    },
};
