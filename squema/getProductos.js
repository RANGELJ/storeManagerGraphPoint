module.exports = {
    type: "@List(Producto)",
    async resolve(root, args, context) {
        // @Error al consultar los productos
        assert["isLogedIn"];
        const productos = context.knex
            .select("id", "descripcion", "costoCompra", "costoVenta")
            .from("productos")
            .where({
                activo: 1,
                idOwner: context.session.idOwner,
            })
            .orderBy("descripcion");
        return productos;
    },
};
