module.exports = async function(root, args, context) {
    // sqlfiles: ["users"],

    const nombreReguex = /[A-Z][a-zA-Z]{2,10}(\s[A-Z][a-zA-Z]{2,10})?/;

    if (! args.nombre ) {
        context.throwError("Debes colocar un nombre");
    }

    if (! nombreReguex.test(args.nombre)) {
        context.throwError("Nombre con formato invalido");
    }

    if (! args.apellidoPaterno ) {
        context.throwError("Debes indicar el apellido paterno");
    }

    if (! nombreReguex.test(args.apellidoPaterno)) {
        context.throwError("Apellido paterno con formato invalido");
    }

    if (! args.apellidoMaterno ) {
        context.throwError("Debes indicar el apellido materno");
    }

    if (! nombreReguex.test(args.apellidoMaterno)) {
        context.throwError("Apellido materno con formato invalido");
    }

    const conn = await context.db("store");
    const [mailusers] = await conn.execute("sql@byEmail", [
        args.email,
    ]);

    if (mailusers.length > 0) {
        context.throwError("El correo electronico indicado ya "
            + "esta siendo usado por otra cuenta");
    }

    return true;
};
