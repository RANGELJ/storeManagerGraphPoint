module.exports = async (root, args, context) => {
    if (context.session.userType !== 1) {
        context.throwError("No eres un administrador");
    }
};
