module.exports = async (root, args, context) => {
    if (context.session.idUser === undefined
        || context.session.idOwner === undefined) {
        context.throwError("No hay una sesion activa");
    }

    const user = await context.knex
        .select("idUsuarioOwner", "tipoDeCuenta", "tipoUsuario")
        .from("usuarios")
        .where({
            activo: 1,
            id: context.session.idUser,
        }).first();

    if (user === undefined) {
        context.throwError("Error, usuario invalido");
    }

    context.session.idOwner = user.idUsuarioOwner;
    context.session.accountType = user.tipoDeCuenta;
    context.session.userType = user.tipoUsuario;
};
